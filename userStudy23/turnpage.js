// // 这里是控制全局js的文件
// // 全局阻止浏览器默认行为
// document.addEventListener("touchstart",function(e){
//     if(e.cancelable){
//         e.preventDefault();
//     }
// },{passive: false})
// // {passive: false}就是告诉前面可能有需要重置行为的代码
// document.addEventListener("touchmove",function(e){
//     if(e.cancelable){
//         e.preventDefault();
//     }
// },{passive: false})

function turnPage(){

    // JavaScript代码
    let curPageIndex=0;
    let pageContainer=document.querySelector("#vis");
    let pageNumber=2;  //页面的数量

// 文档的视窗高度（这里就是一屏的高度）
    let cWidth=document.documentElement.clientWidth;
// 设置页面容器的margin-top为合适的值，让其显示在视野中
    function toPage(){

        pageContainer.style.transition="all .5s ease";
        pageContainer.style.marginLeft=-curPageIndex*cWidth+"px";
        // 变化完成后去掉过渡效果 !
        pageContainer.addEventListener("transitionend",function(){
            pageContainer.style.transition="none";
        },{once:true})
    }
    toPage();

    pageContainer.ontouchstart=function(e){

        let x=e.changedTouches[0].clientX;  //手指按下的纵坐标
        pageContainer.ontouchmove=function(e){
            let dis=e.changedTouches[0].clientX-x;  //计算距离
            // 计算page-container的margin-top
            let mLeft=-curPageIndex*cWidth+dis;
            if(mLeft>0){
                mLeft=0;
            }else if(mLeft<-(pageNumber-1)*cWidth){
                mLeft=-(pageNumber-1)*cWidth;
            }
            console.log(pageNumber);
            // 实时改变位置
            pageContainer.style.marginLeft=mLeft+"px";
        }
        pageContainer.ontouchend=function(e){
            let dis=e.changedTouches[0].clientX-x;
            // 如果滑动距离实在太短，就回到滑动前的位置状态
            if(dis>0 && curPageIndex>0){
                curPageIndex--;
                toPage()
            }else if(dis<0 && curPageIndex<pageNumber-1){
                curPageIndex++;
                toPage()
            }

            // 手指离开后，取消监听事件
            pageContainer.ontouchmove=null;
            pageContainer.ontouchend=null;
        }
    }
}
